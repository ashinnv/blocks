package blocks

import (
	"crypto/sha256"
	"strings"
	"time"
)

type ModDiffs struct {
	Uname   string       //Who modified this?
	ModTime time.Time    //When modified this?
	DiffDat map[int]byte //Index along file data where the change occurred and the byte that would be there instead
}

type Block struct {
	FileDat    []byte                 //Actual file data
	FileNam    string                 //File name
	FileHash   []byte                 //SHA256 hash of file
	ColorRange map[int]float32        //Histogram of image types. Int is hue from red to violet and float is the value amount
	AddedDate  time.Time              //Time file was added
	ModDiffs   map[time.Time]ModDiffs //Times that this block was modified and the username who did so
	ClassTags  []string               //Classifier tags for classifiers such as image classification or audio classification
	UClassTags []string               //User-submitted classification tags such as "my favorite" or "why is this picture of her on your computer?"
}

func GenBlock(uname string, name string, fDat []byte) Block {

	diffMapDed := make(map[time.Time]ModDiffs)
	nameSplit := strings.Split(uname, ".")
	fileExt := nameSplit[len(nameSplit)]

	_, hist := checkGenHistogram(fileExt, fDat) // Function is currently spiked

	var retBlk Block = Block{
		fDat,
		name,
		genSHA256(fDat),
		hist,
		time.Now(),
		diffMapDed,
		[]string{""},
		[]string{""},
	}

	return retBlk
}

//If the incoming data is an image, return true and a histogram of that image. Else, return false and a blank histogram
func checkGenHistogram(fileExt string, fDat []byte) (bool, map[int]float32) {

	var mtMap = make(map[int]float32)
	return false, mtMap

	/*

		fileExts := []string{"jpg", "jpeg", "png", "gif", "tiff", "tif", "bmp", "dib", "webp", "psd", "raw", "arw", "cr2", "nrw", "k25", "heif", "heic", "indd", "indd", "indt", "jp2", "j2k", "jpf", "jpx", "jpm", "mj2", "svg", "svgz", "ai", "eps"}
		isImg := false
		for _, ex := range fileExts {
			if fileExt == ex {
				isImg = true
				break
			}
		}

		if isImg == false {
			return isImg, mtMap
		}

		var opts jpeg.Options
		opts.Quality = 1

		err = jpeg.Encode
	*/
}

func genSHA256(input []byte) []byte {
	h := sha256.New()
	h.Write(input)
	return h.Sum(nil)
}
